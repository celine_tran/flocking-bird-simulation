# FlockingProject_Celine_Cathy

In this project, we use the Monogame framework to create a simulation of the flocking behavior of birds based on on Craig Reynold’s Boids program (Link: http://www.red3d.com/cwr/boids/)
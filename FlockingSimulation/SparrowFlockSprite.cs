using FlockingBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic ;

namespace FlockingSimulation{

    ///<summary>This class is responsible for displaying all the sparrows</summary>
    ///<author>Cathy Tham</author>
    public class SparrowFlockSprite:DrawableGameComponent{
	    private int originOfRotation=10;
        private SpriteBatch spriteBatch;
        private Texture2D sparrowTexture;
        private Game1 game;

        private List<Sparrow> sparrowListObj;

        ///<summary>
        ///This constructor initialize new Game1 and Sparrow objects to run the game
        ///</summary>
        ///<param name="game)">Game1, game class of the game</param>
        ///<param name="sparrowListObj)">List<Sparrow> list of the sparrows</param>
        public SparrowFlockSprite(Game1 game, List<Sparrow> sparrowListObj): base(game)
        {
            // TODO: Construct any child components here
            this.game=game;
            this.sparrowListObj=sparrowListObj;
        }

        ///<summary>Initialize()  is used to initialize</summary>
        public override void Initialize()
        { 
            // TODO: Add your initialization logic here   
            base.Initialize();
        }

        ///<summary>LoadContent() loads all the textures</summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            sparrowTexture = game.Content.Load<Texture2D>("sparrow");

            // TODO: use this.Content to load your game content here
            base.LoadContent();
        }

        ///<summary>Update() updates the game</summary>
        ///<param name="gameTime)">GameTime of the game</param> 
        public override void Update(GameTime gameTime)
        {  
            // TODO: Add your update logic here   
          base.Update(gameTime);
        }

        ///<summary>Draw() used to draw the sparrows to the game</summary>
        ///<param name="gameTime)">GameTime of the game</param>
        public override void Draw(GameTime gameTime)
        { 
            spriteBatch.Begin();
             foreach(var sparrow in sparrowListObj){
            //spriteBatch.Draw(sparrowTexture, new Microsoft.Xna.Framework.Vector2(sparrow.Position.Vx, sparrow.Position.Vy), null, Color.White, sparrow.Rotation, new Microsoft.Xna.Framework.Vector2(sparrow.Velocity.Vx,sparrow.Velocity.Vy), 1, SpriteEffects.None, 0f);
            spriteBatch.Draw(sparrowTexture, new Microsoft.Xna.Framework.Vector2(sparrow.Position.Vx, sparrow.Position.Vy), null, Color.White, sparrow.Rotation, new Microsoft.Xna.Framework.Vector2(originOfRotation,originOfRotation), 1, SpriteEffects.None, 0f);
            }
            spriteBatch.End();

            // TODO: Add your drawing code here  
            base.Draw(gameTime);
        }



    }
}
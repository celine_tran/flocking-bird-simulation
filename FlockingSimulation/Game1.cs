﻿using FlockingBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FlockingSimulation
{
    ///<summary>
    ///This class contains contains a World field and fields for each of the 3 sprite objects. 
    ///</summary>
    ///<author>Cathy Tham</author>
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private SparrowFlockSprite sparrowFlockSprite;

        private RavenSprite ravenSprite;

        private World world;


        ///<summary>
        ///This constructor initialize new instance to run the game
        ///It also creates the World object and sets the field
        ///</summary>
        public Game1()
        {
            world = new World();
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        ///<summary>
        ///Initialize() initialize the SparrowFlockSprite and RavenSprite objects.
        ///It sets the width and height of the application window based on the static properties in World
        ///</summary>
        protected override void Initialize()
        {
            //TODO: Add your initialization logic here
            //set the width and heigh
            _graphics.PreferredBackBufferHeight = World.Height;
            _graphics.PreferredBackBufferWidth = World.Width;
            _graphics.ApplyChanges();

            //instantiate the sprites
            sparrowFlockSprite=new SparrowFlockSprite(this, world.SparrowListObj);
            ravenSprite=new RavenSprite(this, world.RavenObj);

            //add the sprites to Game1’s components
            Components.Add(sparrowFlockSprite);
            Components.Add(ravenSprite);

            base.Initialize();
        }


        ///<summary>
        ///LoadContent() load the content of the game
        ///</summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        ///<summary>
        ///Update() invokes the World object’s Update() method.
        ///</summary>
        ///<param name="gameTime)">GameTime of the game</param> 
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            world.Update();

            base.Update(gameTime);
        }

        ///<summary>
        ///Draw() used to draw the game
        ///It's not used here
        ///</summary>
        ///<param name="gameTime)">GameTime of the game</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}

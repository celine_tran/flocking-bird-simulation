using FlockingBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlockingSimulation{

    ///<summary>This class is responsible for displaying the raven</summary>
    ///<author>Cathy Tham</author>
    public class RavenSprite:DrawableGameComponent{
        private int originOfRotation=10;
	    	//to render
        private SpriteBatch spriteBatch;
        private Texture2D ravenTexture;
        private Game1 game;

        private Raven ravenObj;

        ///<summary>This constructor initialize new Game1 and Raven objects to run the game</summary>
        ///<param name="game)">Game1, game class of the game</param> 
        ///<param name="ravenObj)">Raven, a raven object</param>
        public RavenSprite(Game1 game, Raven ravenObj): base(game)
        {
            // TODO: Construct any child components here
            this.game=game;
            this.ravenObj=ravenObj;
        }

        ///<summary>Initialize() is used to initialize </summary>
        public override void Initialize()
        { 
            // TODO: Add your initialization logic here   
            base.Initialize();
        }

        ///<summary>LoadContent() loads all the textures</summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ravenTexture = game.Content.Load<Texture2D>("Raven");

            // TODO: use this.Content to load your game content here
            base.LoadContent();
        }

        ///<summary>Update() updates the game</summary>
        ///<param name="gameTime)">GameTime of the game</param> 
        public override void Update(GameTime gameTime)
        {  
            // TODO: Add your update logic here   
          base.Update(gameTime);
        }

        ///<summary>Draw() used to draw the raven to the window</summary>
        ///<param name="gameTime)">GameTime of the game</param>
        public override void Draw(GameTime gameTime)
        { 
            spriteBatch.Begin();
            //spriteBatch.Draw(ravenTexture, new Microsoft.Xna.Framework.Vector2(100, 100),Color.White);;
            spriteBatch.Draw(ravenTexture, new Microsoft.Xna.Framework.Vector2(ravenObj.Position.Vx, ravenObj.Position.Vy), null, Color.White, ravenObj.Rotation, new Microsoft.Xna.Framework.Vector2(originOfRotation,originOfRotation), 1, SpriteEffects.None, 0f);
            spriteBatch.End();

            // TODO: Add your drawing code here  
            base.Draw(gameTime);
        }



    }
}
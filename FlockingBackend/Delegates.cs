using System.Collections.Generic ;

namespace FlockingBackend
{		
		///<author>Cathy Tham</author>
    ///<summary> File that declares list of delegates that will be used to subscribe the sparrows/raven to the flock. 
    ///</summary>

    ///<summary>This delegate will be used to raise the event to calculate the movement vector for each sparrow/the raven
    ///</summary>
    ///<param name="sparrowlist"> A list of sparrows</param>
    public delegate void CalculateMoveVector(List<Sparrow> sparrowlist);

    ///<summary>This delegate will raise the event required to move the sparrows/raven.
    ///</summary>
    public delegate void MoveBird();

    ///<summary>This will be used to raise the event required to calculate the vector amount to steer to flee the chasing raven.
    ///</summary>
    ///<param name="raven"> A Raven object</param>
    public delegate void CalculateRavenAvoidance(Raven raven);

}
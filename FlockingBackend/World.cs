﻿using System;
using System.Collections.Generic ;

namespace FlockingBackend
{		
  ///<author>Cathy Tham</author>
	///<author>Celine Tran</author>
	///<summary>Class <c>World</c> represents the “world” of our 2D birds. This class is where everything is stored. </summary>
  public class World
  {
    ///<summary>A private Flock object field.</summary>
    private Flock flockObj;
				
		///<summary>Gets a list of sparrows.</summary>
    public List<Sparrow> SparrowListObj{ get;}
				
		///<summary>Gets the raven object.</summary>
    public Raven RavenObj{ get;}
				
		///<summary>Gets a value indicating the initial count.</summary>
    public static int InitialCount{get;}
				
		///<summary>Gets a value indicating the width.</summary>
    public static int Width{get;}
				
		///<summary>Gets a value indicating the height.</summary>
    public static int Height{get; }
			
		///<summary>Gets a value indicating the max speed.</summary>
    public static int MaxSpeed{get;}
				
		///<summary> Gets a value indicating the neighbour radius.</summary>
    public static int NeighbourRadius{get;}
				
		///<summary> Gets a value indicating the avoidance radius.</summary>
    public static int AvoidanceRadius{get;}


    ///<summary>
    ///This static constructor that sets the following static getter only properties.
    ///</summary>
    static World(){
      //number of sparrows
      InitialCount = 150;
      //Width of the canvas (“world”)
      Width = 1000; 
      //Height of the canvas
      Height = 500;
      //Max speed of the birds
      MaxSpeed = 4; 
      //Radius used to determine if a bird is a neighbour
      NeighbourRadius = 100; 
      //Radius used to determine if a bird is too close
      AvoidanceRadius=50;
    }
     
    ///<summary>
    ///This constructor initializes the Flock object and List<Sparrow>.
    ///The list is populated with Sparrows at random position
    ///</summary>
    public World(){
      //Random rnd = new Random();
      //Initialize Flock object
      flockObj=new Flock();
      
      //The list should be populated with Sparrows at random positions on the screen
      SparrowListObj=new List<Sparrow>();
      for(int i=0; i<InitialCount; i++){
        Sparrow sparrow=new Sparrow();
        SparrowListObj.Add(sparrow);
      }

      //Each Sparrow in the list must also subscribe to the Flock by calling the Flock’s Subscribe method with the 3 event handler methods of the Sparrow class
      foreach(Sparrow sparrow in SparrowListObj){
        flockObj.Subscribe(sparrow.CalculateBehaviour,sparrow.Move,sparrow.CalculateRavenAvoidance);
      }
			
      //initialize the Raven object.
			RavenObj=new Raven();
      //Subscribe it to the Flock via the Flock object’s subscribe method and pass the 2 event handler methods to the subscribe method
      flockObj.Subscribe(RavenObj.CalculateBehaviour,RavenObj.Move);
        
    }
      
    ///<summary>
    ///This method invokes the Flock’s RaiseMoveEvents method and passes the List<Sparrow> and Raven object to it
    ///</summary>
    public void Update(){
      flockObj.RaiseMoveEvents(SparrowListObj, RavenObj);
    }
      
  }
}

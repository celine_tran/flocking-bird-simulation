using System;
using System.Collections.Generic;

namespace FlockingBackend
{
		///<author>Cathy Tham</author>
		///<author>Celine Tran</author>
    ///<summary> This class is used to represent a single raven. </summary>
    public class Raven:Bird
    {
			///<summary>Constructor invoking the bird class's constructor. </summary>
        public Raven():base()
        {
        }
				
				///<summary> Constructor invoking the bird class's test constructor. </summary>
        public Raven(int positionVx, int positionVy, int velocityVx, int velocityVy)
        :base(positionVx,positionVy,velocityVx,velocityVy)
        {
        }

        ///<value> Property <c>Rotation</c> to rotate the raven to face the direction it is moving toward.</value>
        public override float Rotation
        {
            get 
            {
                return (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
            }
        }


        ///<summary> This method is an event handler to calculate the amountToSteer vector for the raven. </summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows) 
        {
            this.amountToSteer = ChaseSparrow(sparrows);
        }

        ///<summary>
        ///This method is the event handler which applies the amountToSteer calculated.
        ///It updates Velocity by adding the amountToSteer to it, updates position by adding velocity to it.
        ///Also, it should make sure the sparrow doesn't fly "outside" the edge by calling AppearOnOppositeSide method
        ///</summary>
        public override void Move(){
						//Update Velocity by adding the amountToSteer to it
						this.Velocity = amountToSteer + this.Velocity;
            this.Position = this.Velocity + this.Position;
            //normalize the velocity
					  this.Velocity=Vector2.NormalizeVector(this.Velocity)*World.MaxSpeed;	
            AppearOnOppositeSide();  
        }
				

        ///<summary> This method is a private helper methods to implement chase sparrows. </summary>
        ///<param name="sparrows">List of sparrows</param>
        public Vector2 ChaseSparrow (List<Sparrow> sparrows){
          	Sparrow sparrowNearest = null;
						Vector2 resultVector = new Vector2(0,0);
						float lastDistanceSquared = 0 ;
          	foreach(Sparrow sparrow in sparrows){
            		float distanceSquared = Vector2.DistanceSquared(this.Position, sparrow.Position);
            		//Check if the squared distance calculated in the previous step is less than the squared avoidance radius 	
            		if (distanceSquared < Math.Pow(World.AvoidanceRadius,2)  && distanceSquared !=0){  
										if(lastDistanceSquared == 0 ){
												lastDistanceSquared = distanceSquared;
												sparrowNearest = sparrow; 
										}
										//if current distance is closer than last distance 
										if(lastDistanceSquared>distanceSquared){
											lastDistanceSquared = distanceSquared;
											//set that current sparrow as the nearest to the raven.
											sparrowNearest = sparrow; 
										}
            		}
						}
            //If there is a sparrow near to the raven
            //Else, just return the default vector 
						if(sparrowNearest != null){
								resultVector = sparrowNearest.Position - this.Position;
								resultVector = Vector2.NormalizeVector(resultVector)*World.MaxSpeed;	
            }
        		return resultVector;
        }
    }
}

using System.Collections.Generic;

namespace FlockingBackend
{
		///<author>Cathy Tham</author>
		///<author>Celine Tran</author>
    ///<summary>
    ///This class is the subscriber class that each bird subscribes to. 
		///The class also raises the events to calculate movement vector and move the birds.
    ///</summary>
    public class Flock
    {
        //raised to calculate the vector amount to steer for each sparrow or raven.
        public event CalculateMoveVector CalcMovementEvent;
        //raised to calculate the amount to steer to flee the raven.
        public event CalculateRavenAvoidance CalcRavenFleeEvent;
        //raised to move the sparrows/raven.
        public event MoveBird MoveEvent;


        ///<summary>	Subscribe method that takes as input delegates instances and subscribes them to the corresponding events.</summary> 
				///<param name="calcMovEvent"> A CalculateMoveVector delegate instance</param>
        ///<param name="moveEvent">A MoveBird delegate instance </param>
				///<param name="calcRavenFleeEvent"> An optional CalculateRavenAvoidance delegate instance</param>
        public void Subscribe(CalculateMoveVector calcMovEvent,MoveBird moveEvent, CalculateRavenAvoidance calcRavenFleeEvent=null){
            CalcMovementEvent+=calcMovEvent;
            MoveEvent+=moveEvent;
            if(calcRavenFleeEvent!=null){
            CalcRavenFleeEvent+=calcRavenFleeEvent;
            }
        }


        ///<summary>
        ///This method raises the calculate and move events.
        ///</summary>
        ///<param name="sparrows">List of Sparrow objects</param>
        ///<param name="raven">A Raven object</param>
        public void RaiseMoveEvents(List<Sparrow> sparrows, Raven raven)
        {
            CalcMovementEvent?.Invoke(sparrows);
            CalcRavenFleeEvent?.Invoke(raven);
						MoveEvent?.Invoke();
        } 
    }
}
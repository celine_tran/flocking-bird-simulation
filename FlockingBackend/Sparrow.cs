using System;
using System.Collections.Generic;
namespace FlockingBackend
{
		///<author>Cathy Tham</author>
		///<author>Celine Tran</author>
    ///<summary> This class is used to represent a single sparrow. </summary>
    public class Sparrow: Bird
    {
				///<summary>Constructor invoking the bird class's constructor. </summary>
        public Sparrow():base()
        {
        }
				
				///<summary>Constructor invoking the bird class's test constructor. </summary>
        public Sparrow(int positionVx, int positionVy, int velocityVx, int velocityVy)
        :base(positionVx,positionVy,velocityVx,velocityVy)
        {
        }
       
        ///<value> Property <c>Rotation</c> to rotate the Sparrow to face the direction it is moving toward.
        ///</value>
        public override float Rotation{
            get 
            {
                return (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
            }
        }

        ///<summary>
        ///This method is an event handler to calculate and set amountToSteer vector using the flocking algorithm rules
        ///by adding to amountToSteer the sum of the vectors returned by Cohesion, Avoidance and Alignment helper methods.
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows){
			this.amountToSteer = Alignment(sparrows) + Cohesion(sparrows) + Avoidance(sparrows);
		}


        ///<summary>
        ///This method is an event handler to calculate and update amountToSteer vector with the amount to steer to flee a chasing raven.
        ///</summary>
        ///<param name="raven">A Raven object</param>
        public void CalculateRavenAvoidance(Raven raven)
        {
            //Add the vector returned by FleeRaven to the amountToSteer vector.
            this.amountToSteer += FleeRaven(raven);
        }


        ///<summary>
        ///The idea of alignment is for every sparrow to align its velocity to the average of the sparrows near it.
        ///This method takes as input a sparrow list and calculates for this Sparrow, how much it should adjust its
        ///velocity to align itself with the nearby sparrows.
        ///</summary>
				///<param name="sparrows"> A list of sparrows. </param>
				///<returns> The resulting vector after alignment.</returns>
        public Vector2 Alignment (List<Sparrow> sparrows){
            Vector2 resultVelocity = new Vector2(0,0);
						Vector2 emptyVector = new Vector2(0,0);
            int count = 0;
            foreach(Sparrow sparrow in sparrows){
                //Check if the squared distance is less than the squared neighbour radius
								var distanceSquared = Vector2.DistanceSquared(this.Position,sparrow.Position);
                if ((distanceSquared < Math.Pow(World.NeighbourRadius,2)) && (distanceSquared !=0) && (this != sparrow)){   
                    //add sparrow’s velocity to the result vector.
                    resultVelocity += sparrow.Velocity;
                    count++;  
                }
            }
            if(count!=0){
                //Then, calculate the average velocity.
                resultVelocity = resultVelocity / count;
								//normalize the average velocity and multiply by maxspeed
								resultVelocity = Vector2.NormalizeVector(resultVelocity) * World.MaxSpeed;
								//subtract this sparrow’s velocity from the calculated average and normalize again
								resultVelocity = resultVelocity - this.Velocity;
								resultVelocity = Vector2.NormalizeVector(resultVelocity);
								//Return the result vector.
								return resultVelocity;
            }
            else{
							//return empty vector if the count is 0
            	return emptyVector;
            }
        }


				///<summary>
				///Cohesion is similar to alignment except here every sparrows adjusts its position to match the average
				///position of the neighbouring sparrows. The algorithm is almost the same as alignment except we are 
				///calculating average position instead of velocity. All the other steps are the same. 
				///</summary>
				///<param name="sparrows"> A list of sparrows. </param>
				///<returns> The resulting vector after Cohesion.</returns>
        public Vector2 Cohesion (List<Sparrow> sparrows){
            Vector2 resultPosition = new Vector2(0,0);
						Vector2 emptyVector = new Vector2(0,0);
            int count = 0;
						//sum position of all sparrows near this Sparrow
            foreach(Sparrow sparrow in sparrows){
                //Check if the squared distance is less than the squared neighbour radius
								var distanceSquared = Vector2.DistanceSquared(this.Position,sparrow.Position);
                if ((distanceSquared <  Math.Pow(World.NeighbourRadius,2)) && (distanceSquared != 0) && (this != sparrow)){
                    resultPosition+=sparrow.Position;
                    count++;
                }
            }
            if(count!=0){
                //Calculate the avg position of the sparrow near this sparrow
                resultPosition = resultPosition / count;
                //Move the sparrow to the average position just calculated
                resultPosition = resultPosition - this.Position;
								//Normalize the result vector and multiply by MaxSpeed
								resultPosition = Vector2.NormalizeVector(resultPosition) * World.MaxSpeed;
								//Get the sparrow’s velocity after displacement
								resultPosition = resultPosition - this.Velocity;
                //Normalize the final vector and return the result
								resultPosition = Vector2.NormalizeVector(resultPosition);
								return resultPosition;
            }
            else{
							//return empty vector if the count is 0
            	return emptyVector;
            }
        }


			///<summary> This method allows every sparrow to try to adjust its velocity to avoid converging into the sparrows near it. </summary>
			///<param name="sparrows"> A list of sparrows. </param>
			///<returns> The resulting vector after Avoidance.</returns>
      public Vector2 Avoidance (List<Sparrow> sparrows){
					Vector2 differencePosition = new Vector2(0,0);
					Vector2 resultVector = new Vector2(0,0);
					Vector2 emptyVector = new Vector2(0,0);
					int count = 0;
					/*For each sparrow in the list of Sparrows, calculate the squared distance between this Sparrow’s position and a sparrow in the list.*/
					foreach(Sparrow sparrow in sparrows){
              //Check if the squared distance is less than the squared neighbour radius and the sparrow is not itself
							var distanceSquared = Vector2.DistanceSquared(this.Position,sparrow.Position);
              if ((distanceSquared < Math.Pow(World.AvoidanceRadius,2)) && (distanceSquared !=0) && (this != sparrow)){
                  //calculate the difference between the two positions by subtracting the other sparrow’s position from this sparrow’s position.
									differencePosition = this.Position - sparrow.Position;
                  //Divide the difference by the distance
									differencePosition = differencePosition / distanceSquared;
									//Add this difference to a result vector
                  resultVector += differencePosition;
									count++;
        	    }
          }
          if(count!=0){
              //calculate the average
              resultVector = resultVector / count;
              //Normalize the average vector and multiply by MaxSpeed.
              resultVector = Vector2.NormalizeVector(resultVector) * World.MaxSpeed;
              /*subtract this sparrow’s velocity from the calculated average and normalize again.*/
              resultVector = resultVector - this.Velocity;
              /*Normalize the final vector and return the result.*/
              resultVector = Vector2.NormalizeVector(resultVector);
							return resultVector;
          }
          else{
							//return empty vector if the count is 0
              return emptyVector;
          }
      }
			
        ///<summary>
				///This method takes as input a Raven object and returns a
				///Vector2 representing the amount to steer to flee the raven.
        ///</summary>
				///<param name="raven"> The raven in question. </param>
				///<returns> The resulting vector after FleeRaven.</returns>
        public Vector2 FleeRaven (Raven raven){
            Vector2 resultVector = new Vector2(0,0);
            //Calculate the squared distance between this Sparrow’s position and the raven.
            var distanceSquared = Vector2.DistanceSquared(this.Position,raven.Position);
            //If the calculated squared distance is less than the squared avoidance radius, 
            if ((distanceSquared < World.AvoidanceRadius * World.AvoidanceRadius) && (distanceSquared != 0)){
                //calculate the difference between the two positions 
                resultVector = this.Position - raven.Position;
                //Divide the difference by the distance.
								resultVector = resultVector / distanceSquared;
								//Normalize the difference just calculated and multiply by Max speed.
                resultVector = Vector2.NormalizeVector(resultVector) * World.MaxSpeed;
            }
						//returns vector that is set to default 0,0
						return resultVector;
        } 
    }
}

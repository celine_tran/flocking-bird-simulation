using System;

namespace FlockingBackend
{
		///<author>Cathy Tham</author>
		///<author>Celine Tran</author>
		///<summary>This struct is represents the vector and is used for the mathematic functionalities. </summary>
    public struct Vector2
    {
				///<summary>Gets a value indicating the vector's x parameter.</summary>
        public float Vx{
            get;
        }
				///<summary>Gets a value indicating the vector's y parameter.</summary>
        public float Vy{
            get;
        }

				///<summary>Constructor initializing a new instance of vector2.</summary>
				///<param name="Vx"> The Vector's x parameter. </param>
				///<param name="Vy"> The Vector's y parameter. </param>
        public Vector2(float Vx, float Vy){
						this.Vx = Vx;
            this.Vy = Vy;
        }

				///<summary>Method that overloads the minus operator.</summary>
				///<param name="a"> The first vector. </param>
				///<param name="b"> The second vector. </param>
				///<returns> The resulting vector from the minus operation.</returns>
				public static Vector2 operator -(Vector2 a, Vector2 b){
						float x,y;
						x = a.Vx - b.Vx;
						y = a.Vy - b.Vy;
						return new Vector2(x,y); 
				}

				///<summary>Method that overloads the plus operator.</summary>
				///<param name="a"> The first vector. </param>
				///<param name="b"> The second vector. </param>
				///<returns> The resulting vector from the plus operation.</returns>
				public static Vector2 operator +(Vector2 a, Vector2 b){
						float x,y;
						x = a.Vx + b.Vx;
						y = a.Vy + b.Vy;
						return new Vector2(x,y); 
				}

				///<summary>Method that overloads the divide operator.</summary>
				///<param name="a"> The first Vector2 value </param>
				///<param name="floatNum"> float value </param>
				///<returns> The resulting vector from the devide operation.</returns>
				public static Vector2 operator /(Vector2 a, float floatNum){
						float x,y;
						x = a.Vx / floatNum;
						y = a.Vy / floatNum;
						return new Vector2(x,y); 
				}


				///<summary>Method that overloads the multiply operator. Multiplies vector and float</summary>
				///<param name="a"> Vector2 value </param>
				///<param name="floatNum">float value</param>
				///<returns> The resulting vector from the multiply operation.</returns>
				public static Vector2 operator *(Vector2 a, float floatNum){
						float x,y;
						x = a.Vx * floatNum;
						y = a.Vy * floatNum;
						return new Vector2(x,y); 
				}

				///<summary>Method that overloads the multiply operator. Multiplies 2 vectors </summary>
				///<param name="a">First Vector2 value </param>
				///<param name="b">Second Vector2 value </param>
				///<returns> The resulting vector from the multiply operation.</returns>
				public static Vector2 operator *(Vector2 a, Vector2 b){
						float x,y;
						x = a.Vx * b.Vx;
						y = a.Vy * b.Vy;
						return new Vector2(x,y); 
				}


				///<summary> 
				///A static DistanceSquared method that takes two Vectors as input and returns a float 
				///representing the squared distance between the two Vectors.</summary>
				///<param name="a">Vector2 value </param>
				///<param name="b"> The second vector. </param>
				///<returns> The resulting vector from the plus operation.</returns>
				public static float DistanceSquared(Vector2 a, Vector2 b){
						float distance;
						distance = (float)(Math.Pow((a.Vx - b.Vx), 2) + Math.Pow((a.Vy - b.Vy), 2));
						return distance;
				}

				///<summary> 
				///A static Normalize method that takes a Vector and returns a new Vector which is normalized.
				///Normalizing a vector means persevering the direction while making the magnitude 1.</summary>
				///<param name="vector"> The vector in question. </param>
				///<returns>A new Vector which is normalized.</returns>
				public static Vector2 NormalizeVector(Vector2 vector){
						float magnitude = (float)Math.Sqrt((vector.Vx*vector.Vx) + (vector.Vy*vector.Vy));
						float x = vector.Vx / magnitude;
						float y = vector.Vy / magnitude;
						return  new Vector2(x,y);
				}
		}
}
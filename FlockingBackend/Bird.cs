using System;
using System.Collections.Generic ;

namespace FlockingBackend
{
		///<author>Cathy Tham</author>
		///<author>Celine Tran</author>
    ///<summary> Abstract class <c>Bird</c> that defines the characters of a bird.  
		///Sparrow and Raven should extend this class. 
    ///</summary>
    public abstract class Bird
    {
        //Properties
        public abstract float Rotation{get;}
        public Vector2 Position{get; protected set;}
        public Vector2 Velocity{get; protected set;}
        protected Vector2 amountToSteer;


				///<summary> Abstract method that calculates the behaviour. </summary>
				///<param name="sparrowList"> A list of sparrows</param>
        public abstract void CalculateBehaviour(List<Sparrow> sparrowList);


        ///<summary> This constructor initializes the Position vector at a random position.
        ///It also initializes the velocity vector to a random value and the remainin Vector2 fields.
        ///</summary>
        public Bird(){
            Random rnd = new Random();
            float randPositionVx=rnd.Next(0, World.Width);
            float randPositionVy=rnd.Next(0, World.Height);
            this.Position=new Vector2(randPositionVx,randPositionVy);
            //Initialize the velocity vector to a random value in the range of -4 to 4
            float randVelocityVx=rnd.Next(-1 * World.MaxSpeed, 5);
            float randVelocityVy=rnd.Next(-1 * World.MaxSpeed, 5);
            this.Velocity=new Vector2(randVelocityVx,randVelocityVy);
						//Initialize all the remaining Vector2 fields to default values i.e.(0,0)
						this.amountToSteer = new Vector2(0,0);
        }


        ///<summary>
        ///This constructor for testing purposes that takes 4 ints/floats as input and sets the Position
				///and Velocity vector using the input parameters 
				///</summary>
        ///<param name="positionVx"> The x parameter of the position vector</param>
				///<param name="positionVy"> The y parameter of the position vector</param>
				///<param name="velocityVx"> The x parameter of the velocity vector</param>
				///<param name="velocityVy"> The y parameter of the velocity vector</param>
        public Bird(int positionVx, int positionVy, int velocityVx, int velocityVy){
            this.Position=new Vector2(positionVx,positionVy);
            this.Velocity=new Vector2(velocityVx,velocityVy);
            this.amountToSteer=new Vector2(0,0);
        }

        ///<summary>
        ///This method makes the sparrow appear on the opposite edge if it flies outside the edge of the screen
        ///</summary>
        protected void AppearOnOppositeSide(){
             if (this.Position.Vx > World.Width){
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if(this.Position.Vx < 0){
                 this.Position = new Vector2(World.Width, this.Position.Vy);
            }
            if (this.Position.Vy > World.Height){
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if(this.Position.Vy < 0){
                this.Position= new Vector2(this.Position.Vx, World.Height);
            }
        }

        ///<summary>
        ///This method is the event handler which applies the amountToSteer calculated.
        ///It updates Velocity by adding the amountToSteer to it, updates position by adding velocity to it.
        ///Also, it should make sure the sparrow doesn't fly "outside" the edge by calling AppearOnOppositeSide method
        ///</summary>
        public virtual void Move(){
						//Update Velocity by adding the amountToSteer to it
						this.Velocity += amountToSteer;
            this.Position += this.Velocity;
            AppearOnOppositeSide(); 
        }
    }
}
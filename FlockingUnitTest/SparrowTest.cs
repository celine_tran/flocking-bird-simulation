using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;

namespace FlockingUnitTest
{
	[TestClass]
  	public class SparrowTest{
			
		[TestMethod]
		public void TestAlignmentReturnsResultVector(){  
			//Arrange 
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(4,5,6,7);
			Sparrow sparrow3 = new Sparrow(8,9,10,11);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = -0.614f;
			float expY = -0.789f;
							
			//Act
			Vector2 result = sparrow1.Alignment(sparrows);
							
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);		
		} 


		[TestMethod]
		public void TestAlignmentReturnsDefaultVector(){  
			//Arrange 
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(110,130,6,7);
			Sparrow sparrow3 = new Sparrow(330,400,10,11);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = 0f;
			float expY = 0f;
							
			//Act
			Vector2 result = sparrow1.Alignment(sparrows);
							
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);		
		}


		[TestMethod]
		public void TestCohesionReturnsResultVector(){  
			//Arrange
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(4,5,6,7);
			Sparrow sparrow3 = new Sparrow(8,9,10,11);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = -0.447f;
			float expY = -0.894f;
						
			//Act
			Vector2 result = sparrow1.Cohesion(sparrows);
				
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);	
		} 

		[TestMethod]
		public void TestCohesionReturnsDefaultVector(){  
			//Arrange
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(2,4,5,6);
			Sparrow sparrow3 = new Sparrow(2,4,5,6);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = 0f;
			float expY = 0f;
					
			//Act
			Vector2 result = sparrow1.Cohesion(sparrows);
					
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);	
		} 
		
		[TestMethod]
		public void  TestAvoidanceReturnsResultVector(){  
			//Arrange
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(4,5,6,7);
			Sparrow sparrow3 = new Sparrow(8,9,10,11);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = -0.728f;
			float expY = -0.684f;
					
			//Act
			Vector2 result = sparrow1.Avoidance(sparrows);
					
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);
		}

		[TestMethod]
		public void  TestAvoidanceReturnsDefaultVector(){  
			//Arrange
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Sparrow sparrow2 = new Sparrow(2,4,5,6);
			Sparrow sparrow3 = new Sparrow(2,4,5,6);
			sparrows.Add(sparrow2);
			sparrows.Add(sparrow3);
			float expX = 0f;
			float expY = 0f;
					
			//Act
			Vector2 result = sparrow1.Avoidance(sparrows);
					
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);
		}  

		[TestMethod]
		public void  TestFleeRaven(){ 
			//Arrange 
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Raven raven = new Raven(10,45,1,6);
			float expX = -0.766f;
			float expY = -3.925f;
				
			//Act
			Vector2 result = sparrow1.FleeRaven(raven);
				
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);
		}

		[TestMethod]
		public void  TestFleeRavenReturnsDefaultVector()
		{  
			//Arrange 
			List<Sparrow> sparrows = new List<Sparrow>();
			Sparrow sparrow1 = new Sparrow(2,4,5,6);
			Raven raven = new Raven(300,400,500,600);
			float expX = 0f;
			float expY = 0f;
			//Act
			Vector2 result = sparrow1.FleeRaven(raven);
		
			//Assert
			Assert.AreEqual(expX, result.Vx, 0.01);
			Assert.AreEqual(expY, result.Vy, 0.01);
		}
    }
}
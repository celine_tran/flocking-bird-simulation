using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;
namespace FlockingUnitTest
{
    [TestClass]
    public class Vector2Test
    {
		[TestMethod]
        public void TestAddingOperator()
        {
						//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2(5,7);
            Vector2 a = new Vector2(3,4);
            Vector2 b = new Vector2(2,3);
						//Act
            Vector2 c = a + b;
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");
		}

		[TestMethod]
        public void TestMinusOperator()
        {		
						//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2(1,1);
            Vector2 a = new Vector2(3,4);
            Vector2 b = new Vector2(2,3);
						//Act
            Vector2 c = a - b;
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");
		}


        [TestMethod]
        public void TestDivideOperator()
        {		//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2(1,4);
            Vector2 a = new Vector2(3,12);
            float b = 3;
						//Act
            Vector2 c = a / b;
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");

        }

 		[TestMethod]
        public void TestMultiplyOperator()
        {
						//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2(9,12);
            Vector2 a = new Vector2(3,4);
            float b = 3;
						//Act
            Vector2 c = a * b;
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");

        }

		[TestMethod]
        public void TestDistanceSquaredOperator()
        {
						//Arrange 
            float actual;
            float expected = 13;
            Vector2 a = new Vector2(3,4);
            Vector2 b = new Vector2(1,1);
						//Act
            float c = Vector2.DistanceSquared(a,b);
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");

        }

		[TestMethod]
        public void TestNormalizerOperator()
        {
						//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2((float)0.70710677,(float)0.70710677);
            Vector2 a = new Vector2(1,1);
						//Act
            Vector2 c = Vector2.NormalizeVector(a);
            actual = c;
						//Assert
            Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");

        }
				
    }
}

using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlockingBackend;

namespace FlockingUnitTest
{
    [TestClass]
    public class BirdTest
    {
         [TestMethod]
        public void TestMoveVelocity()
        {   
						//Arrange 
            Vector2 actual;
            Vector2 expected = new Vector2(4,1);
            Bird bird=new Sparrow(111,442,4,1);
						//Act
            bird.Move();
            actual=bird.Velocity;
						//Assert
						Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");
				}	

          [TestMethod]
        public void TestMovePosition()
        {   
						//Arrange
            Vector2 actual;
            Vector2 expected = new Vector2(115,443);
            Bird bird=new Sparrow(111,442,4,1);
						//Act
            bird.Move();
            actual=bird.Position;
						//Assert
       			Assert.AreEqual(expected,actual,"There was an error. Actual result does not match expected result.");
				}
    }
}

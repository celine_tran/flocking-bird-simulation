using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;
//Test if it returns the right method
namespace FlockingUnitTest
{
	[TestClass]
    public class FlockTest
    {
				[TestMethod]
				public void  TestReturnsRaiseMoveEventsSparrow()
				{ 
						//Arrange 
						Flock flockObj=new Flock();
						Raven raven=new Raven(3,4,5,6);
						List<Sparrow> sparrows = new List<Sparrow>();
						Sparrow sparrow1 = new Sparrow(28,4,5,6);
						Sparrow sparrow2 = new Sparrow(2,4,5,6);
						Sparrow sparrow3 = new Sparrow(20,3,4,6);
						sparrows.Add(sparrow1);
						sparrows.Add(sparrow2);
						sparrows.Add(sparrow3);
						float sparrowVxBefore=sparrow2.Position.Vx;
						float sparrowVyBefore=sparrow2.Position.Vy;
						//Act
						foreach(Sparrow sparrow in sparrows){
								flockObj.Subscribe(sparrow.CalculateBehaviour,sparrow.Move,sparrow.CalculateRavenAvoidance);
						}
						flockObj.Subscribe(raven.CalculateBehaviour,raven.Move);
						flockObj.RaiseMoveEvents(sparrows,raven);
						//Assert
						Assert.AreNotEqual(sparrow2.Position.Vx, sparrowVxBefore);
						Assert.AreNotEqual(sparrow2.Position.Vy, sparrowVyBefore);
				}

				[TestMethod]
				public void  TestReturnsRaiseMoveEventsRaven()
				{ 
						//Arrange 
						Flock flockObj=new Flock();
						Raven raven=new Raven(3,4,5,6);
						float ravenVxBefore=raven.Position.Vx;
						float ravenVyBefore=raven.Position.Vy;
						List<Sparrow> sparrows = new List<Sparrow>();
						Sparrow sparrow1 = new Sparrow(28,4,5,6);
						Sparrow sparrow2 = new Sparrow(2,4,5,6);
						sparrows.Add(sparrow1);
						sparrows.Add(sparrow2);
						//Act
						foreach(Sparrow sparrow in sparrows){
							flockObj.Subscribe(sparrow.CalculateBehaviour,sparrow.Move,sparrow.CalculateRavenAvoidance);
						}
						flockObj.Subscribe(raven.CalculateBehaviour,raven.Move);
						flockObj.RaiseMoveEvents(sparrows,raven);
						//Assert
						Assert.AreNotEqual(raven.Position.Vx, ravenVxBefore);
						Assert.AreNotEqual(raven.Position.Vy, ravenVyBefore);
				}
		}
}
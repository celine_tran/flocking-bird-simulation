using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FlockingBackend;
using System;

namespace FlockingUnitTest
{
		[TestClass]
    public class RavenTest
    {
        [TestMethod]
      	public void  TestChaseSparrowWhenNear()
       	{
					 	//Arrange 
            Raven raven=new Raven(3,4,5,6);
						List<Sparrow> sparrows = new List<Sparrow>();
						Sparrow sparrow1 = new Sparrow(28,4,5,6);
						Sparrow sparrow2 = new Sparrow(2,4,5,6);
						Sparrow sparrow3 = new Sparrow(20,3,4,6);
						sparrows.Add(sparrow1);
						sparrows.Add(sparrow2);
						sparrows.Add(sparrow3);
						float expX = -4;
       			float expY = 0;
	
						//Act
						Vector2 result = raven.ChaseSparrow(sparrows);
				
						//Assert
						Assert.AreEqual(expX, result.Vx, 0.01);
						Assert.AreEqual(expY, result.Vy, 0.01);
				}  

        [TestMethod]
      	public void  TestChaseSparrowWhenNotNear()
       	{
					 //Arrange 
            Raven raven=new Raven(3,4,5,6);
						List<Sparrow> sparrows = new List<Sparrow>();
						Sparrow sparrow1 = new Sparrow(500,600,700,900);
						Sparrow sparrow2 = new Sparrow(300,400,500,600);
						Sparrow sparrow3 = new Sparrow(550,650,750,950);
						sparrows.Add(sparrow2);
						sparrows.Add(sparrow3);
						float expX = 0f;
       			float expY = 0f;
	
						//Act
						Vector2 result = raven.ChaseSparrow(sparrows);
				
						//Assert
						Assert.AreEqual(expX, result.Vx, 0.01);
						Assert.AreEqual(expY, result.Vy, 0.01);
				}  
    }
}